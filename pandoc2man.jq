#!/usr/bin/jq -f

def fixheaders:
    if .t == "Header" then
        .c[2][] |= (if .t == "Str" then .c |= ascii_upcase else . end)
    else
        .
    end;

def fixlinks:
    if type == "object" then
        if .t == "Link" then
            if .c[2][0][0:7] == "mailto:" then . else .c[1][] end
        else
            map_values(fixlinks)
        end
    else if type == "array" then
            map(fixlinks)
        else
            .
        end
    end;

{
    "pandoc-api-version"
  , meta
  , blocks: .blocks | map(fixheaders | fixlinks)
}
