srcdir ?= .
builddir ?= build
prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin
libdir ?= $(exec_prefix)/lib
sitelib ?= $(libdir)/site_perl
datarootdir ?= $(prefix)/share
mandir ?= $(datarootdir)/man
man1dir ?= $(mandir)/man1
systemd_userunitdir ?= $(libdir)/systemd/user

CSS ?= /usr/share/javascript/bootstrap4/css/bootstrap.css
HTML_TEMPLATE ?= $(srcdir)/doc/template.html

PROGRAMS = $(addprefix $(builddir)/,interimap pullimap)
HTML_FILES = $(patsubst $(srcdir)/doc/%.md,$(builddir)/doc/%.html,$(wildcard $(srcdir)/doc/*.md))
MANUAL_FILES = $(patsubst $(srcdir)/doc/%.md,$(builddir)/doc/%,$(wildcard $(srcdir)/doc/*.[1-9].md))
SERVICE_FILES = $(patsubst $(srcdir)/%.service,$(builddir)/%.service,$(wildcard $(srcdir)/*.service))

all: all-nodoc manual
all-nodoc: $(PROGRAMS) $(SERVICE_FILES)

doc: manual html
manual: $(MANUAL_FILES)
html: $(HTML_FILES)

$(PROGRAMS): $(builddir)/%: $(srcdir)/%
	@mkdir -vp $(dir $@)
	perl -Te "print \"\$$_\\0\" foreach @INC;" | grep -Fxzq -e "$(sitelib)" && prefix="#" || prefix=""; \
	sed -r "0,/^(use\\s+\lib\\s+)([\"'])[^\"']*\\2\\s*;/ s||$$prefix\\1\"$(sitelib)\";|" <"$<" >"$@"
	chmod --reference="$<" -- "$@"

# upper case the headers and remove the links
$(MANUAL_FILES): $(builddir)/doc/%: $(srcdir)/doc/%.md
	@mkdir -vp $(dir $@)
	pandoc -f markdown -t json -- "$<" | $(srcdir)/pandoc2man.jq | pandoc -s -f json -t man -o "$@"

$(SERVICE_FILES): $(builddir)/%.service: $(srcdir)/%.service
	@mkdir -vp $(dir $@)
	sed "s|@bindir@|$(bindir)|" <"$<" >"$@"

testcerts:
	$(srcdir)/tests/certs/generate

check: check-interimap check-pullimap
check-interimap: $(builddir)/interimap testcerts
	INTERIMAP_I=$(srcdir)/lib INTERIMAP_PATH=$(builddir) $(srcdir)/tests/run-all interimap.list
check-pullimap: $(builddir)/pullimap testcerts
	INTERIMAP_I=$(srcdir)/lib INTERIMAP_PATH=$(builddir) $(srcdir)/tests/run-all pullimap.list

installcheck: installcheck-interimap installcheck-pullimap
installcheck-interimap: testcerts
	INTERIMAP_I="" INTERIMAP_PATH=$(bindir) $(srcdir)/tests/run-all interimap.list
installcheck-pullimap: testcerts
	INTERIMAP_I="" INTERIMAP_PATH=$(bindir) $(srcdir)/tests/run-all pullimap.list

release:
	@if ! git -C $(srcdir) diff --quiet HEAD -- Changelog interimap pullimap lib/Net/IMAP/InterIMAP.pm; then \
		echo "Dirty state, refusing to release!" >&2; \
		exit 1; \
	fi
	VERS=$$(dpkg-parsechangelog -l $(srcdir)/Changelog -SVersion 2>/dev/null) && \
		if git -C $(srcdir) rev-parse -q --verify "refs/tags/v$$VERS" >/dev/null; then echo "tag exists" 2>/dev/null; exit 1; fi && \
		sed -ri "0,/^( -- .*)  .*/ s//\\1  $(shell date -R)/" $(srcdir)/Changelog && \
		sed -ri "0,/^(our\\s+\\\$$VERSION\\s*=\\s*)'[0-9.]+'\\s*;/ s//\\1'$$VERS';/" \
			-- $(srcdir)/interimap $(srcdir)/pullimap && \
		sed -ri "0,/^(package\\s+Net::IMAP::InterIMAP\\s+)v[0-9.]+\\s*;/ s//\\1v$$VERS;/" \
			-- $(srcdir)/lib/Net/IMAP/InterIMAP.pm && \
		sed -ri "0,/^(use\\s+Net::IMAP::InterIMAP\\s+)[0-9.]+(\\s|\\$$)/ s//\\1$$VERS\\2/" \
			-- $(srcdir)/interimap $(srcdir)/pullimap && \
		git -C $(srcdir) commit -m "Prepare new release v$$VERS." \
			-- Changelog interimap pullimap lib/Net/IMAP/InterIMAP.pm && \
		git -C $(srcdir) tag -sm "Release version $$VERS" "v$$VERS"

$(HTML_FILES): $(builddir)/doc/%.html: $(srcdir)/doc/%.md $(HTML_TEMPLATE)
	@mkdir -vp $(dir $@)
	mtime="$$(git -C $(srcdir) --no-pager log -1 --pretty="format:%ct" -- "$<" 2>/dev/null)"; \
	[ -n "$$mtime" ] || mtime="$$(date +%s -r "$<")"; \
	pandoc -sp -f markdown -t html+smart --css=$(CSS) --template=$(HTML_TEMPLATE) \
		--variable=date:"$$(LC_TIME=C date +"Last modified on %a, %d %b %Y at %T %z" -d @"$$mtime")" \
		--variable=keywords:"interimap" \
		--variable=lang:"en" \
		--variable=parent:"$(if $(filter $@,$(builddir)/doc/index.html),,./index.html)" \
		--output="$@" -- "$<"

INSTALL ?= install
INSTALL_PROGRAM ?= $(INSTALL)
INSTALL_DATA ?= $(INSTALL) -m0644

install: install-nodoc
	-$(INSTALL_DATA) -vDt $(DESTDIR)$(man1dir) $(builddir)/doc/interimap.1 $(builddir)/doc/pullimap.1
	$(INSTALL_DATA) -vDt $(DESTDIR)$(datarootdir)/doc/pullimap $(srcdir)/pullimap.sample
	$(INSTALL_DATA) -vDt $(DESTDIR)$(datarootdir)/doc/interimap $(srcdir)/interimap.sample \
		$(srcdir)/doc/getting-started.md $(srcdir)/doc/multi-account.md $(srcdir)/README

install-nodoc: all-nodoc
	$(INSTALL_PROGRAM) -vDt $(DESTDIR)$(bindir) $(builddir)/interimap $(builddir)/pullimap
	$(INSTALL_DATA) -vDT $(srcdir)/lib/Net/IMAP/InterIMAP.pm $(DESTDIR)$(sitelib)/Net/IMAP/InterIMAP.pm
	$(INSTALL_DATA) -vDt $(DESTDIR)$(systemd_userunitdir) $(SERVICE_FILES)

uninstall:
	rm -vf -- $(DESTDIR)$(bindir)/interimap $(DESTDIR)$(man1dir)/interimap.1 $(DESTDIR)$(systemd_userunitdir)/interimap*.service
	rm -vf -- $(DESTDIR)$(bindir)/pullimap $(DESTDIR)$(man1dir)/pullimap.1 $(DESTDIR)$(systemd_userunitdir)/pullimap*.service
	rm -vf -- $(DESTDIR)$(sitelib)/Net/IMAP/InterIMAP.pm
	rm -rvf -- $(DESTDIR)$(datarootdir)/doc/interimap $(DESTDIR)$(datarootdir)/doc/pullimap

clean:
	rm -vf -- $(PROGRAMS) $(MANUAL_FILES) $(HTML_FILES) $(SERVICE_FILES)
	rm -vf -- $(srcdir)/tests/certs/*.key $(srcdir)/tests/certs/*.crt $(srcdir)/tests/certs/*.pem
	-rmdir -vp --ignore-fail-on-non-empty -- $(builddir)/doc

.PHONY: all all-nodoc manual html doc release testcerts \
	check check-interimap check-pullimap \
	install install-nodoc \
	installcheck installcheck-interimap installcheck-pullimap \
	uninstall clean
