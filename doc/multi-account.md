% Multi-remote setup for InterIMAP
% [Guilhem Moulin](mailto:guilhem@fripost.org)

This document describes the setup of InterIMAP against two remotes
accounts, a *personal* account at `imap.example.net`, and a *work*
account at `imap.example.com`.  (The same setup can be adapted to remote
accounts or more.)  These remotes accounts are to be synchronized
locally, each being mirrored to its own local namespace in a
bidirectional fashion.

We assume familiarity with the [Getting Started] tutorial.
Prerequisites, such as installing dependencies, creating directories,
etc., won't be repeated here.


Layout
======

For this example we assume the *personal* account has the following
mailbox hierarchy (with delimiter `/`):

    INBOX
    debian
    debian/debian-devel
    debian/debian-project
    debian/debian-security
    archives

And the *work* account the following mailbox hierarchy (with delimiter `.`):

    INBOX
    INBOX.todo
    attic

These remote mailbox hierarchies are to be mirrored locally as:

    INBOX
    perso/INBOX
    perso/debian
    perso/debian/debian-devel
    perso/debian/debian-project
    perso/debian/debian-security
    perso/archives
    work/INBOX
    work/INBOX/todo
    work/attic

Using again (arbitrarily) `/` as local hierarchy delimiter.  (Since 0.5
each server can choose its own hierarchy delimiter independently on the
other ones, and even change it — for instance after changing the server
software — without breaking synchronization.)

Note that there are alternative layouts (one can for instance replace
the prefix `perso/` with the empty string), however for this example
we'll focus on the above layout.


Local Dovecot configuration
===========================

Although it's not required (it's possible to use the default namespace
for everything), we propose to use a dedicated [IMAP namespace][RFC
2342] for each remote account to mirror locally:

 * It provides better isolation of the locally mirrored accounts: one
   can use entirely independent mail storage for instance, and even
   different hierarchy delimiters.
 * One can use `perso/INBOX` as the local `INBOX`.  (Per [RFC 3501] the
   `INBOX` mailbox has a special nature.)  Or even alias a particular
   namespace to the default namespace.  That being said, one should use
   aliases with care as they might have undesired side effects, such as
   `perso/INBOX` missing from `LIST` responses when the mailbox is used
   as the local `INBOX`; or confusion between `perso/work/foo` and
   `work/foo` when `perso/` is aliased to the default namespace.

We define 3 namespaces: a default namespace holding the local `INBOX`,
as well as a dedicated namespace — with a suitable prefix — for each
remote account to mirror locally.  Consult the [Dovecot namespaces]
documentation for more information.  Note that mailboxes (and the
messages they contain) residing in the default namespace won't be copied
over: they will only live on the local instance.

    $ cat >${XDG_CONFIG_HOME:-~/.config}/dovecot/dovecot.conf <<-EOF
		ssl = no
		namespace {
		    location  = maildir:~/Mail
		    inbox     = yes
		    separator = /
		}
		namespace perso {
		    prefix    = perso/
		    location  = maildir:~/Mail/perso
		    separator = /
		}
		namespace work {
		    prefix    = work/
		    location  = maildir:~/Mail/work
		    separator = /
		}
	EOF


We can see the three namespaces using the `` `~/.local/bin/dovecot-imap` ``
wrapper defined in the [Getting Started] tutorial.

    $ ~/.local/bin/dovecot-imap
    S: * PREAUTH [CAPABILITY IMAP4rev1 …] Logged in as myuser
    C: a NAMESPACE
    S: * NAMESPACE (("" "/")("work/" "/")("perso/" "/")) NIL NIL
    S: a OK Namespace completed (0.001 + 0.000 secs).
    C: b LIST "" "*"
    S: * LIST (\Noselect \HasNoChildren) "/" work
    S: * LIST (\Noselect \HasNoChildren) "/" perso
    S: * LIST (\HasNoChildren) "/" INBOX
    S: b OK List completed (0.001 + 0.000 secs).
    C: q LOGOUT
    S: * BYE Logging out
    S: q OK Logout completed (0.001 + 0.000 secs).


InterIMAP configuration
=======================

We use a dedicated [`interimap`(1)] instance for each remote account to
synchronize, starting with the *personal* account.  Again, see the
[Getting Started] guide for details about the configuration file and its
settings.

    $ install -m0600 /dev/null ${XDG_CONFIG_HOME:-~/.config}/interimap/personal
<!-- -->
    $ cat >${XDG_CONFIG_HOME:-~/.config}/interimap/personal <<-EOF
		database = personal.db

		[local]
		type = tunnel
		list-reference = perso/
		command = exec ~/.local/bin/dovecot-imap

		[remote]
		type = imaps
		host = imap.example.net
		username = myname
		password = xxxxxxxx
	EOF
<!-- -->
    $ interimap --config=personal
    Creating new schema in database file …/personal.db
    database: Created mailbox INBOX
    local: Created mailbox perso/INBOX
    […]

And similarly for the *work* account:

    $ install -m0600 /dev/null ${XDG_CONFIG_HOME:-~/.config}/interimap/work
<!-- -->
    $ cat >${XDG_CONFIG_HOME:-~/.config}/interimap/work <<-EOF
		database = work.db

		[local]
		type = tunnel
		list-reference = work/
		command = exec ~/.local/bin/dovecot-imap

		[remote]
		type = imaps
		host = imap.example.com
		username = myname2
		password = xxxxxxxx
	EOF
<!-- -->
    $ interimap --config=work
    Creating new schema in database file …/work.db
    database: Created mailbox INBOX
    local: Created mailbox work/INBOX
    […]

The local mail storage should now have the desired local layout:

    $ ~/.local/bin/dovecot-imap
    S: * PREAUTH [CAPABILITY IMAP4rev1 …] Logged in as myuser
    C: b LIST "" "*"
    S: * LIST (\Noselect \HasChildren) "/" work
    S: * LIST (\HasNoChildren) "/" work/attic
    S: * LIST (\HasChildren) "/" work/INBOX
    S: * LIST (\HasChildren) "/" work/INBOX
    S: * LIST (\HasNoChildren) "/" work/INBOX/todo
    S: * LIST (\Noselect \HasChildren) "/" perso
    S: * LIST (\HasNoChildren) "/" perso/INBOX
    S: * LIST (\HasChildren) "/" perso/debian
    S: * LIST (\HasNoChildren) "/" perso/debian/debian-security
    S: * LIST (\HasNoChildren) "/" perso/debian/debian-project
    S: * LIST (\HasNoChildren) "/" perso/debian/debian-devel
    S: * LIST (\HasNoChildren) "/" perso/archives
    S: * LIST (\HasNoChildren) "/" INBOX
    S: a OK List completed (0.003 + 0.000 + 0.002 secs).
    C: q LOGOUT
    S: * BYE Logging out
    S: q OK Logout completed (0.001 + 0.000 secs).

Template user unit for systemd are provided in order to run these
[`interimap`(1)] instances as services:

    $ systemctl --user enable --now interimap@{personal,work}.service

[Getting Started]: getting-started.html
[RFC 2342]: https://tools.ietf.org/html/rfc2342
[RFC 3501]: https://tools.ietf.org/html/rfc3501
[Dovecot namespaces]: https://doc.dovecot.org/configuration_manual/namespace/
[`interimap`(1)]: interimap.1.html
