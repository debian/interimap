% Build instructions
% [Guilhem Moulin](mailto:guilhem@fripost.org)

On Debian 10 (codename *Buster*) and later, installing [`interimap`(1)]
is a single command away:

    $ sudo apt install interimap

This document is for those who are running other systems, and/or who
wish to install from [source](https://git.guilhem.org/interimap).


Dependencies
============

[`interimap`(1)](interimap.1.html) depends on Perl ≥5.20 and the
following Perl modules:

  * [`Compress::Raw::Zlib`](https://perldoc.perl.org/Compress/Raw/Zlib.html) (*core module*)
  * [`Config::Tiny`](https://metacpan.org/pod/Config::Tiny)
  * [`DBI`](https://metacpan.org/pod/DBI)
  * [`DBD::SQLite`](https://metacpan.org/pod/DBD::SQLite)
  * [`Errno`](https://perldoc.perl.org/Errno.html) (*core module*)
  * [`Getopt::Long`](https://perldoc.perl.org/Getopt/Long.html) (*core module*)
  * [`MIME::Base64`](https://perldoc.perl.org/MIME/Base64.html) (*core module*) — if authentication is required
  * [`List::Util`](https://perldoc.perl.org/List/Util.html) (*core module*)
  * [`Net::SSLeay`](https://metacpan.org/pod/Net::SSLeay) ≥1.86_06
  * [`POSIX`](https://perldoc.perl.org/POSIX.html) (*core module*)
  * [`Socket`](https://perldoc.perl.org/Socket.html) (*core module*)
  * [`Time::HiRes`](https://perldoc.perl.org/Time/HiRes.html) (*core module*) — if `logfile` is set

On Debian GNU/Linux systems, the dependencies can be installed with the
following command:

    $ sudo apt install libconfig-tiny-perl libdbd-sqlite3-perl libnet-ssleay-perl

Additional packages are required in order to run the test suite:

    $ sudo apt install dovecot-imapd dovecot-lmtpd openssl procps sqlite3 xxd
<!-- -->
    $ make check

(Note also that the test suite needs to bind to TCP ports 10024, 10143
and 10993 on the loopback interface.)

Generate documentation
======================

Yet another set of packages is needed to generate the documentation:

    $ sudo apt install jq pandoc

Run `` `make manual` `` (or just `` `make` ``) in order to generate the
manuals.  You'll find them at `build/doc/*.[1-9]`.  Then use for
instance `` `man -l build/doc/interimap.1` `` to read your local copy of
the [`interimap`(1)] manual.

The HTML documentation can be built with `` `make html` ``.  By default
HTML files are generated at `build/doc/*.html`, and a local CSS file is
used — namely `/usr/share/javascript/bootstrap4/css/bootstrap.css`,
shipped the by [`libjs-bootstrap`](https://tracker.debian.org/libjs-bootstrap)
package.  But this is configurable: use for instance

    $ make html builddir="$XDG_RUNTIME_DIR/interimap" \
		CSS="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"

to generate the HTML documentation under `$XDG_RUNTIME_DIR/interimap/doc`
using a remote CSS file.

The `doc` target generates all documentation, manuals as well as HTML
pages.


Installation without root privileges
====================================

By default `` `make install` `` installs [`interimap`(1)] under
`/usr/local`, hence requires root privileges.  However another prefix
can be used in order to perform the (un)installation as an unprivileged
user.  For instance

    $ install -m0700 -vd ${XDG_DATA_HOME:-~/.local/share}/interimap
    $ make install-nodoc \
		prefix=$HOME/.local \
		sitelib=${XDG_DATA_HOME:-~/.local/share}/interimap/lib \
		systemd_userunitdir=${XDG_DATA_HOME:-~/.local/share}/systemd/user

skips documentation and installs

  * executables into `~/.local/bin` (instead of `/usr/local/bin`);
  * libraries into `$XDG_DATA_HOME/interimap/lib` or `~/.local/share/interimap/lib`
    (instead of `/usr/local/lib/site_perl`); and
  * [systemd user unit files][`systemd.unit`(5)] into `$XDG_DATA_HOME/systemd/user`
    or `~/.local/share/systemd/user` (instead of `/usr/local/lib/systemd/user`).

Note that for uninstallation one must call `` `make uninstall prefix=…` ``
with the very same assignment(s) used for installation.

[`systemd.unit`(5)]: https://www.freedesktop.org/software/systemd/man/systemd.unit.html

Build custom Debian packages
============================

Debian GNU/Linux users can also use [`gbp`(1)] from
[`git-buildpackage`](https://tracker.debian.org/pkg/git-buildpackage) in
order to build their own packages:

    $ git checkout debian/latest
    $ gbp buildpackage

Alternatively, for the development version:

    $ git checkout debian/latest
    $ git merge master
    $ gbp buildpackage --git-force-create --git-upstream-tree=BRANCH


[`interimap`(1)]: interimap.1.html
[`gbp`(1)]: https://manpages.debian.org/git-buildpackage/gbp.1.en.html
