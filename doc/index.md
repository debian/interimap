% InterIMAP & PullIMAP
% [Guilhem Moulin](mailto:guilhem@fripost.org)

InterIMAP synchronizes emails and their metadata between a remote IMAP
server and local storage.  By leveraging the [*Quick Mailbox
Resynchronization*][RFC 7162] IMAP extension, it can offer [*much better
performance*](benchmark.html) than tools such as [OfflineIMAP].

PullIMAP retrieves messages from an IMAP mailbox and deliver them
locally.  It can use the the [*IDLE*][RFC 2177] IMAP extension to reduce
both latency and network traffic.

[RFC 2177]: https://tools.ietf.org/html/rfc2177
[RFC 7162]: https://tools.ietf.org/html/rfc7162
[OfflineIMAP]: https://www.offlineimap.org/

General documentation
---------------------

 * [Getting started with InterIMAP](getting-started.html)
 * [Multi-remote setup for InterIMAP](multi-account.html)
 * [InterIMAP benchmark metrics and comparison](benchmark.html)
 * [Presentation at DebConf19](https://debconf19.debconf.org/talks/78-interimap-the-case-for-local-imap-servers-and-fast-bidirectional-synchronization/)

Manuals (HTML versions)
-----------------------

  * [`interimap`(1)](interimap.1.html) — Fast bidirectional
    synchronization for IMAP servers
  * [`pullimap`(1)](pullimap.1.html) — Pull mails from an IMAP mailbox
    and deliver them to an SMTP session

Resources for developers
------------------------

  * [Source-code repository](https://git.guilhem.org/interimap)
  * [Build instructions](build.html)
  * [Test environment setup](development.html)
