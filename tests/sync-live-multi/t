TIMEOUT=60

# mailbox list (as seen on local) and alphabet
declare -a MAILBOXES=( "INBOX" ) ALPHABET=()
str="#+-0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz"
for ((i=0; i < ${#str}; i++)); do
    ALPHABET[i]="${str:i:1}"
done

declare -a TARGETS=( "local" "remote1" "remote2" "remote3" )


# create databases
interimap_init "remote1"
interimap_init "remote2"
interimap_init "remote3"

# start long-lived interimap processes
declare -a PID=()
trap 'ptree_abort ${PID[@]}' EXIT INT TERM
interimap --config="config1" --watch=1 & PID+=( $! )
interimap --config="config2" --watch=1 & PID+=( $! )
interimap --config="config3" --watch=1 & PID+=( $! )

timer=$(( $(date +%s) + TIMEOUT ))
while [ $(date +%s) -le $timer ]; do
    # create new mailbox with 10% probability
    if [ $(shuf -n1 -i0-9) -eq 0 ]; then
        u="$(shuf -n1 -e -- "${TARGETS[@]}")" # choose target at random
        case "$u" in
            local) ns="$(shuf -n1 -e "foo/" "bar/" "baz/")";;
            remote1) ns="foo/";;
            remote2) ns="bar/";;
            remote3) ns="baz/";;
            *) error "Uh?";;
        esac

        m=
        d=$(shuf -n1 -i1-3) # random depth
        for (( i=0; i < d; i++)); do
            l=$(shuf -n1 -i1-16)
            m="${m:+$m/}$(shuf -n "$l" -e -- "${ALPHABET[@]}" | tr -d '\n')"
        done
        MAILBOXES+=( "$ns$m" )
        case "$u" in
            local)   m="$ns$m";;
            remote1) m="${m//\//^}";;
            remote2) m="${m//\//\\}";;
            remote3) m="${m//\//\?}";;
            *) error "Uh?";;
        esac
        doveadm -u "$u" mailbox create -- "$m"
    fi

    # EXPUNGE some messages
    u="$(shuf -n1 -e -- "${TARGETS[@]}")" # choose target at random
    n="$(shuf -n1 -i0-3)"
    while read guid uid; do
        doveadm -u "$u" expunge mailbox-guid "$guid" uid "$uid"
    done < <(doveadm -u "$u" search all | shuf -n "$n")

    # mark some existing messages as read (toggle \Seen flag as unlike other
    # flags it's easier to query and check_mailboxes_status checks it)
    u="$(shuf -n1 -e -- "${TARGETS[@]}")" # choose target at random
    n="$(shuf -n1 -i0-9)"
    while read guid uid; do
        a="$(shuf -n1 -e add remove replace)"
        doveadm -u "$u" flags "$a" "\\Seen" mailbox-guid "$guid" uid "$uid"
    done < <(doveadm -u "$u" search all | shuf -n "$n")

    # select at random a mailbox where to deliver some messages
    u="$(shuf -n1 -e "local" "remote")" # choose target at random
    m="$(shuf -n1 -e -- "${MAILBOXES[@]}")"
    if [ "$u" = "remote" ]; then
        case "$m" in
            foo/*) u="remote1"; m="${m#foo/}"; m="${m//\//^}";;
            bar/*) u="remote2"; m="${m#bar/}"; m="${m//\//\\}";;
            baz/*) u="remote3"; m="${m#baz/}"; m="${m//\//\?}";;
            INBOX) u="$(shuf -n1 -e "remote1" "remote2" "remote3")";;
            *) error "Uh? $m";;
        esac
    fi

    # deliver between 1 and 5 messages to the chosen mailbox
    n="$(shuf -n1 -i1-5)"
    for (( i=0; i < n; i++)); do
        sample_message | deliver -u "$u"  -- -m "$m"
    done

    # sleep a little bit (sometimes beyond --watch timer, sometimes not)
    s=$(shuf -n1 -i1-1500)
    [ $s -ge 1000 ] && s="$(printf "1.%03d" $((s-1000)))" || s="$(printf "0.%03d" $s)"
    sleep "$s"
done

# wait a little longer so interimap has time to run loop() again and
# synchronize outstanding changes, then terminate the processes we
# started above
sleep 5

ptree_abort ${PID[@]}
trap - EXIT INT TERM

# check that the mailbox lists match
diff -u --label="local/mailboxes" --label="remote1/mailboxes" \
    <( doveadm -u "local"   mailbox list | sed -n "s,^foo/,,p" | sort ) \
    <( doveadm -u "remote1" mailbox list | tr '^' '/'          | sort )
diff -u --label="local/mailboxes" --label="remote2/mailboxes" \
    <( doveadm -u "local"   mailbox list | sed -n "s,^bar/,,p" | sort ) \
    <( doveadm -u "remote2" mailbox list | tr '\\' '/'         | sort )
diff -u --label="local/mailboxes" --label="remote3/mailboxes" \
    <( doveadm -u "local"   mailbox list | sed -n "s,^baz/,,p" | sort ) \
    <( doveadm -u "remote3" mailbox list | tr '?' '/'          | sort )

for m in "${MAILBOXES[@]}"; do
    case "$m" in
        foo/*) u="remote1"; mb="${m#foo/}"; mr="${mb//\//^}";;
        bar/*) u="remote2"; mb="${m#bar/}"; mr="${mb//\//\\}";;
        baz/*) u="remote3"; mb="${m#baz/}"; mr="${mb//\//\?}";;
        INBOX) continue;;
        *) error "Uh? $m";;
    esac
    blob="x'$(printf "%s" "$mb" | tr "/" "\\0" | xxd -c256 -ps)'"
    check_mailbox_status2 "$blob" "$m" "$u" "$mr"
done

# vim: set filetype=sh :
